package com.willian.teste.repository;

import com.willian.teste.entities.domain.ProductCosif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCosifRepository extends JpaRepository<ProductCosif, String> {

}
