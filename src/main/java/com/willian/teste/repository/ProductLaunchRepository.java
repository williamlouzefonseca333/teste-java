package com.willian.teste.repository;

import com.willian.teste.entities.domain.ProductLaunch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductLaunchRepository extends JpaRepository<ProductLaunch, Long>, PagingAndSortingRepository<ProductLaunch, Long> {

    Page<ProductLaunch> findAll(Pageable pageReq);

}
