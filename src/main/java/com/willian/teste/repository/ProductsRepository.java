package com.willian.teste.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.willian.teste.entities.domain.Product;

public interface ProductsRepository extends JpaRepository<Product, String>{
	
	@Query(name = "find-product-codes-list-distinct", value = "SELECT COD_PRODUTO from PRODUTO", nativeQuery = true)
	List<String> getAllDistinctProductCodesNative();
}
