package com.willian.teste.services.impl;

import com.willian.teste.dto.request.ProductLaunchRequestDTO;
import com.willian.teste.entities.domain.Product;
import com.willian.teste.entities.domain.ProductCosif;
import com.willian.teste.entities.domain.ProductLaunch;
import com.willian.teste.repository.ProductLaunchRepository;
import com.willian.teste.services.ProductCosifService;
import com.willian.teste.services.ProductLaunchService;
import com.willian.teste.services.ProductsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.NotActiveException;
import java.util.List;

@Service
@AllArgsConstructor
public class ProductLaunchServiceImpl implements ProductLaunchService {

    private final ProductLaunchRepository productLaunchRepository;

    private final ProductCosifService productCosifService;
    private final ProductsService productsService;

    @Override
    public List<ProductLaunch> getProductLaunchPaginated(int page, int size, String sortDir, String sort) {
        PageRequest pageReq
                = PageRequest.of(page, size, Sort.Direction.fromString(sortDir), sort);

        return productLaunchRepository.findAll(pageReq).getContent();
    }

    @Override
    @Transactional
    public void createProductLaunch(ProductLaunchRequestDTO productLaunchRequestDTO) throws NotActiveException {

        Product product = productsService.getProductById(productLaunchRequestDTO.getProductCode());
        ProductCosif productCosif = productCosifService.findById(productLaunchRequestDTO.getCosifCode());

        ProductLaunch productLaunch = new ProductLaunch();

        productLaunch.setProduct(product);
        productLaunch.setProductCosif(productCosif);
        productLaunch.setManualLaunchNumber(getNextManualLaunchNumber());


    }

    private String getNextManualLaunchNumber() {
        return "123";
    }
}
