package com.willian.teste.services.impl;

import java.util.List;

import com.willian.teste.converter.ProductConverter;
import com.willian.teste.dto.request.ProductDTO;
import com.willian.teste.entities.domain.Product;
import com.willian.teste.exceptions.ExceptionHandler;
import org.springframework.stereotype.Service;

import com.willian.teste.repository.ProductsRepository;
import com.willian.teste.services.ProductsService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ProductsServiceImpl implements ProductsService{

	private final ProductsRepository productsRepository;
	
	@Override
	public List<String> getProductsCodes() {
		return productsRepository.getAllDistinctProductCodesNative();
	}

	@Override
	public void createProduct(ProductDTO productDto) {
		productsRepository.save(ProductConverter.convert(productDto));
	}

	@Override
	public Product getProductById(String id) {
		return productsRepository.findById(id)
				.orElseThrow(ExceptionHandler.productNotFoundException(id));
	}

}
