package com.willian.teste.services.impl;

import com.willian.teste.entities.domain.ProductCosif;
import com.willian.teste.exceptions.ExceptionHandler;
import com.willian.teste.repository.ProductCosifRepository;
import com.willian.teste.services.ProductCosifService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductCosifServiceImpl implements ProductCosifService {

    private final ProductCosifRepository productCosifRepository;

    @Override
    public ProductCosif findById(String id) {
        return productCosifRepository.findById(id)
                .orElseThrow(ExceptionHandler.cosifNotFoundException(id.toString()));
    }

    @Override
    public List<ProductCosif> findAll() {
        return productCosifRepository.findAll();
    }
}
