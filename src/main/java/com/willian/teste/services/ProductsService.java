package com.willian.teste.services;

import com.willian.teste.dto.request.ProductDTO;
import com.willian.teste.entities.domain.Product;

import java.io.NotActiveException;
import java.util.List;

public interface ProductsService {
	
	List<String> getProductsCodes();

	void createProduct(ProductDTO productDto);

	Product getProductById(String id) throws NotActiveException;
}
