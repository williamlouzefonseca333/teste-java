package com.willian.teste.services;

import com.willian.teste.entities.domain.ProductCosif;
import java.util.List;

public interface ProductCosifService {

    ProductCosif findById(String id);

    List<ProductCosif> findAll();
}
