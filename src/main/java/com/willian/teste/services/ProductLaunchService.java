package com.willian.teste.services;

import com.willian.teste.dto.request.ProductLaunchRequestDTO;
import com.willian.teste.entities.domain.ProductLaunch;

import java.io.NotActiveException;
import java.util.List;

public interface ProductLaunchService {

    public List<ProductLaunch> getProductLaunchPaginated(int page, int size, String sortDir, String sort);

    void createProductLaunch(ProductLaunchRequestDTO productLaunchRequestDTO) throws NotActiveException;
}
