package com.willian.teste.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ProductNotFoundException extends RuntimeException{
    public ProductNotFoundException(String productId){
        super("Product " + productId + " does not exist.");
    }
}