package com.willian.teste.exceptions;

import lombok.extern.log4j.Log4j2;

import java.util.function.Supplier;

@Log4j2
public class ExceptionHandler {
    public static Supplier<ProductNotFoundException> productNotFoundException(String msg) {
        return () -> {
            log.error(msg);
            return new ProductNotFoundException(msg);
        };
    }
    public static Supplier<CosifNotFoundException> cosifNotFoundException(String msg) {
        return () -> {
            log.error(msg);
            return new CosifNotFoundException(msg);
        };
    }
}