package com.willian.teste;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import lombok.AccessLevel;
import lombok.Generated;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootApplication
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class TesteApplication {
	
	private final Environment env;
	
	@PostConstruct
	public void initApplictaion() {
		log.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
	}

	@Generated
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TesteApplication.class);
		
		app.run(args);
	}

}
