package com.willian.teste.dto.request;

import com.willian.teste.enums.StatusEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO {

    private String productDescription;

    private StatusEnum productStatus;
}
