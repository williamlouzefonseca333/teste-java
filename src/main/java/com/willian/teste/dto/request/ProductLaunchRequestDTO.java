package com.willian.teste.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductLaunchRequestDTO {
    private String month;
    private String year;
    private String productCode;
    private String description;
    private Float value;
    private String cosifCode;
}
