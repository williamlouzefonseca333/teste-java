package com.willian.teste.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductLaunchResponseDTO {
	
	private String launchMonth;
	private String launchYear;
	private String productCode;
	private String productDescription;
	private String launchNumber;
	private String generalDescription;
	private String productValue;
}
