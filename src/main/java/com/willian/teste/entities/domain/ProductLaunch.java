package com.willian.teste.entities.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "MOVIMENTO_MANUAL")
@Getter
@Setter
public class ProductLaunch {
	
	@Id()
	@Column(name = "COD_MOVIENTO_MANUAL")
	String manualLaunchCode;
	
	@Column(name = "DAT_MES")
	String manualLaunchMonth;
	
	@Column(name = "DATE_ANO")
	String manualLaunchYear;

	@Column(name = "DAT_MOVIMENTO")
	Date manualLaunchDate;
	
	@Column(name = "COD_USUARIO")
	String userCode;
	
	@Column(name = "VAL_VALOR")
	Float manualLaunchValue;

	@Column(name = "DES_DESCRICAO")
	String manualLaunchDescription;

	@Column(name = "NUM_LANCAMENTO")
	String manualLaunchNumber;

	@ManyToOne()
	@JoinColumn(name = "COD_PRODUTO", insertable = false, updatable = false)
	Product product;

	@ManyToOne()
	@JoinColumn(name = "COD_COSIF", insertable = false, updatable = false)
	ProductCosif productCosif;
}
