package com.willian.teste.entities.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.willian.teste.enums.StatusEnum;

@Entity
@Table(name = "PRODUTO_COSIF")
public class ProductCosif {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "COD_PRODUTO_COSSIF")
	String productCossifCode;
	
	@Column(name = "COD_PRODUTO")
	String productCode;
	
	@Column(name = "COD_COSIF")
	String cosifCode;
	
	@Column(name = "COD_CASSIFICACAO")
	String classifictionCossifCode;
	
	@Column(name = "STA_STATUS")
	StatusEnum status;
}
