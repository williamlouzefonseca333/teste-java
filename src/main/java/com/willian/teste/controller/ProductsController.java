package com.willian.teste.controller;

import com.willian.teste.dto.request.ProductDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.willian.teste.services.ProductsService;

import lombok.extern.log4j.Log4j2;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/products")
@Log4j2
@RequiredArgsConstructor
public class ProductsController {

	private final ProductsService productsService;

	@GetMapping("/current-products-codes")
	@ResponseBody
	public ResponseEntity<?> getCurrentProductsCodes() throws ParseException {
		
		log.info("ProductsController - returning list of product codes");
		
		return ResponseEntity.ok(productsService.getProductsCodes());
		
	}

	@PostMapping()
	@ResponseBody
	ResponseEntity createProducts(@RequestBody @Valid ProductDTO productDTO) throws ParseException {

		log.info(productDTO);

		productsService.createProduct(productDTO);

		return ResponseEntity.status(HttpStatus.CREATED).build();

	}
}
