package com.willian.teste.controller;

import com.willian.teste.converter.ProductLaunchDTOConverter;
import com.willian.teste.dto.request.ProductLaunchRequestDTO;
import com.willian.teste.services.ProductLaunchService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.willian.teste.dto.response.ProductLaunchResponseDTO;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("launches")
public class ProductLaunchController {

	private final ProductLaunchService productLaunchService;

	@GetMapping(params = { "page", "size" })
	@ResponseBody
	public ResponseEntity<List<ProductLaunchResponseDTO>> getAllProducts(
			@RequestParam("page") int page,
			 @RequestParam("size") int size,
			 @PathVariable("sortDir") String sortDir,
			 @PathVariable("sort") String sort){

		return ResponseEntity.ok(productLaunchService.getProductLaunchPaginated(page, size, sortDir, sort)
				.stream()
				.map(productLaunch -> {
					return ProductLaunchDTOConverter.convert(productLaunch);
				})
				.collect(Collectors.toList()));
	}

//	@PostMapping
//	@ResponseStatus(HttpStatus.CREATED)
//	@ResponseBody
//	public ResponseEntity<?> createProductLaunch(@RequestBody ProductLaunchRequestDTO productLaunchRequestDTO) throws ParseException {
//
//	}
}
