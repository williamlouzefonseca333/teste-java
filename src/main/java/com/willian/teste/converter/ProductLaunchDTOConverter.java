package com.willian.teste.converter;

import com.willian.teste.dto.response.ProductLaunchResponseDTO;
import com.willian.teste.entities.domain.ProductLaunch;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ProductLaunchDTOConverter {

    public static ProductLaunchResponseDTO convert(ProductLaunch productLaunch){
        return new ProductLaunchDTOConverter().convertFrom(productLaunch);
    }

    public ProductLaunchResponseDTO convertFrom(ProductLaunch productLaunch){

        log.info(productLaunch);

        ProductLaunchResponseDTO productLaunchResponseDTO = new ProductLaunchResponseDTO();

        productLaunchResponseDTO.setProductDescription(productLaunch.getProduct().getProductDescription());
        productLaunchResponseDTO.setLaunchMonth(productLaunch.getManualLaunchMonth());
        productLaunchResponseDTO.setLaunchYear(productLaunch.getManualLaunchYear());
        productLaunchResponseDTO.setGeneralDescription(productLaunch.getManualLaunchDescription());
        productLaunchResponseDTO.setProductCode(productLaunch.getProduct().getProductCode());
        productLaunchResponseDTO.setLaunchNumber(productLaunch.getManualLaunchNumber());

        return productLaunchResponseDTO;
    }
}
