package com.willian.teste.converter;

import com.willian.teste.dto.request.ProductDTO;
import com.willian.teste.entities.domain.Product;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ProductConverter{

    public static Product convert(ProductDTO productDTO){
        return new ProductConverter().convertFrom(productDTO);
    }

    public Product convertFrom(ProductDTO productDTO){

        log.info(productDTO);

        Product product = new Product();

        product.setProductDescription(productDTO.getProductDescription());
        product.setStatus(productDTO.getProductStatus());

        log.info(product);

        return product;
    }
}
